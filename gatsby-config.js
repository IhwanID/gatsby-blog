module.exports = {
  siteMetadata: {
    title: `Gatsby Pomade`,
    siteUrl: `https://www.gatsbyjs.org`,
    description: `Blazing fast modern site generator for Hair`,
  },
  plugins: [
    `gatsby-transformer-remark`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages`,
      },
    },
  ],
}
