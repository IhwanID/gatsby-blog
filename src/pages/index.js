import React, { Component } from "react"
import { graphql, Link } from "gatsby"
import Header from "../components/Header"

const Layout = ({data}) => {
    const {edges} = data.allMarkdownRemark
    return (
        <div>
            <Header/>
            {edges.map(edge => {
                const {frontmatter} = edge.node
                return <div key={frontmatter.path}><Link to={frontmatter.path}>{frontmatter.title}</Link></div>
            })}
            <br/>
            <Link to='/tags'>Cari berdasarkan Tags</Link>
        </div>
    )
}

export const query = graphql`
query homepageQuery{
  allMarkdownRemark(sort:{order:ASC, fields:[frontmatter___date]}) {
    edges {
      node {
        frontmatter {
          title
          tags
          path
          date
        }
      }
    }
  }
}
`
export default Layout
