import React, { Component } from "react"
import { graphql, Link } from "gatsby"

const Template = ({data, pageContext}) => {
    const {prev, next} = pageContext
    const {markdownRemark} = data
    const title = markdownRemark.frontmatter.title
    const htm = markdownRemark.html
    return (
        <div>
            <h2>{title}</h2>
            <div dangerouslySetInnerHTML={{__html:htm}}></div>
            <br/>
            {next && <Link to={next.frontmatter.path}>Next</Link>}
            <br/>
            {prev && <Link to={prev.frontmatter.path}>Prev</Link>}
        </div>
    )
}

export const query = graphql`
query($pathSlug: String){
    markdownRemark(frontmatter: {path: {eq: $pathSlug}}){
        html
        frontmatter{
            title
        }
    }
}
`

export default Template